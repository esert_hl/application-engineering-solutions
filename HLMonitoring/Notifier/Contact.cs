﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HLMonitoring.Notifier
{
    public class Contact
    {
        public string Name { get; set; }
        public EmailGroup EmailGroup { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public DateTime LastNotified { get; set; }
    }

    public enum EmailGroup
    {
        BusinessHours,
        Urgent
    }
}
