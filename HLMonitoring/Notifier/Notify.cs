﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using Extensions;
using System.Web;

namespace HLMonitoring.Notifier
{
    public static class Notify
    {
        private static List<Contact> Contacts = new List<Contact>();
        private static EmailConfiguration EConfiguration;

        /// <summary>
        /// Use to load the configuration. Pass an <see cref="EmailConfiguration"/> and list of <see cref="Contact"/>
        /// </summary>
        /// <param name="Conf">EmailConfiguration</param>
        /// <param name="Contacts">Contacts List</param>
        public static void LoadConfiguration(EmailConfiguration Conf, List<Contact> _Contacts)
        {
            Contacts.Clear();
            EConfiguration = Conf;
            foreach(var C in _Contacts)
            {
                if (!Notify.Contacts.Any(t => t.Email.Equals(C.Email, StringComparison.OrdinalIgnoreCase) && t.EmailGroup.Equals(C.EmailGroup)))
                    Notify.Contacts.Add(C);
            }
        }

        /// <summary>
        /// Once the configuration is loaded, notify the contacts. 
        /// </summary>
        /// <param name="C">Content <see cref="Content"/></param>
        public static void NotifyContacts(Content C)
        {
            Contacts.ForEach(t=> SendNotification(C,ref t));           
        }

        private static void SendNotification(Content C,ref Contact CT)
        {
            if (CT.LastNotified < DateTime.Now.AddHours(-1))
            {
                using (SmtpClient Client = new SmtpClient())
                {
                    Client.Port = EConfiguration.Port;
                    Client.DeliveryMethod = SmtpDeliveryMethod.Network;
                    Client.UseDefaultCredentials = false;
                    Client.Host = EConfiguration.Server;
                    Client.Credentials = new NetworkCredential(EConfiguration.UserName, EConfiguration.Password);

                    using (var Mail = new MailMessage(C.FromAddress, CT.Email))
                    {
                        Mail.Subject = C.Subject;

                        if (C.ContentType.Contains("html", StringComparison.OrdinalIgnoreCase))
                        {
                            Mail.IsBodyHtml = true;
                        }

                        Mail.Body = C.EmailBody;

                        if (C.Attachments != null)
                        {
                            foreach (var Attachment in C.Attachments)
                                Mail.Attachments.Add(new Attachment(Attachment, MimeMapping.GetMimeMapping(Attachment)));
                        }

                        Client.Send(Mail);
                        CT.LastNotified = DateTime.Now;
                    }
                }
            }

        }
    }
}
