﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HLMonitoring.Notifier
{
    public class Content
    {
        public string Subject { get; set; }
        public string EmailBody { get; set; }
        public string ContentType { get; set; }
        public string FromAddress { get; set; }
        public List<string> Attachments { get; set; }
    }
}
