﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Scheduler
{
    public class Scheduler
    {
        private TaskConcept _Concept;
        public Scheduler(TaskConcept Concept)
        {
            _Concept = Concept;
        }

        public void StartTask()
        {
            _Concept.ToRun();
            Thread.Sleep(_Concept.DelayInSeconds);

            //Soon to implement TPL
            
        }
    }
}
