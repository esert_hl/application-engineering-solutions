﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Scheduler
{
    public class TaskConcept
    {
        public Action ToRun { get; set; }
        public int DelayInSeconds { get; set; }
        public CancellationToken Token { get; set; }
    }
}
