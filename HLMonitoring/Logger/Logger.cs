﻿using System;
using System.IO;
using Newtonsoft.Json;
using System.Data.SqlClient;
using Extensions;

namespace Logger
{
    public static class Logger
    {
        /// <summary>
        /// Log to the table based on the type provided.
        /// </summary>
        /// <typeparam name="T">Table Type. The table and type must have matching properties</typeparam>
        /// <param name="Context">The database context that will be provided</param>
        /// <param name="TableToLog">The Log information</param>
        public static void Log<T>(SqlConnection Context, T Log) where T : class
        {
            try
            {
                Context.DynamicallyInsert<T>(Log);
            }
            catch (Exception e)
            {
                File.AppendAllText($"{AppDomain.CurrentDomain.BaseDirectory}Log-{DateTime.Now.ToString("MMddyy hhmm")}.log", $"Error Logging:{JsonConvert.SerializeObject(Log)} \nError:{e.ToString()}{Environment.NewLine}");
            }
        }

        public static void Log<T>(SqlConnection Context, T Log, string TableName)
        {

        }

        public static void LogToFile<T>(T Log)
        {
            try
            {
                File.AppendAllText($"{AppDomain.CurrentDomain.BaseDirectory}ActionLogs-{DateTime.Now.ToString("MMddyy")}.log", $"Action Logged at {DateTime.Now}: {JsonConvert.SerializeObject(Log)}{Environment.NewLine}");
            }catch
            {
                // cannot do nothing really
            }
        }
    }
}
