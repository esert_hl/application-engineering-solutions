﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Dapper;

namespace Extensions
{
    public static class Extensions
    {
        public static bool Contains(this string source, string Content, StringComparison Comparison)
        {
            if (string.IsNullOrEmpty(source))
                return false;

            return source.IndexOf(Content) > 0 ? true : false;
        }

        private class TableStructure
        {
            public string TableName { get; set; }
            public string SchemaName { get; set; }
        }
        /// <summary>
        /// The table name must match the type name. Meaning, I do not think you will be able to use tables other than the dbo schema. I will do my due diligence to actually implement a searc but pleace beware of the errors.This supports only SQL Server. Identity inserts would fail since this method does not accommodate it.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="Connection"></param>
        /// <param name="ToInsert"></param>
        public static void DynamicallyInsert<T>(this SqlConnection Connection, T ToInsert) where T : class
        {
            using (Connection)
            {
                var TypeName = ToInsert.GetType().Name;
                var Structure = GetStructure(Connection,TypeName);
                var InsertStatement = $"Insert into [{Structure.Item1.SchemaName}].[{Structure.Item1.TableName}](";
                var ValuesStatement = "values(";
                var Params = new DynamicParameters();
                foreach (PropertyInfo Prop in ToInsert.GetType().GetProperties())
                {
                    if (Structure.Item2.Any(t => t.Equals(Prop.Name, StringComparison.OrdinalIgnoreCase)))
                    {
                        InsertStatement += $"{Prop.Name.ToLower()},";
                        ValuesStatement += $"@{Prop.Name.ToLower()}";
                        Params.Add($"@{Prop.Name.ToLower()}", new DbString { Value = Prop.GetValue(ToInsert).ToString(), IsAnsi = true });
                    }
                }

                InsertStatement = InsertStatement.TrimEnd(',');
                InsertStatement += ")";
                ValuesStatement = ValuesStatement.TrimEnd(',');
                ValuesStatement += ")";
                var Statement = $"{InsertStatement} {ValuesStatement}";
                
                Connection.Query(Statement, Params);

            }
        }
        /// <summary>
        /// The table name needs to be without the schema. 
        /// </summary>
        /// <param name="Connection"></param>
        /// <param name="DataMapping"></param>
        /// <param name="TableName"></param>
        public static void DynamicallyInsert(this SqlConnection Connection, Dictionary<string, object> DataMapping, string TableName)
        {
            using (Connection)
            {
                var Structure = GetStructure(Connection, TableName);

                string InsertStatement = $"insert into [{TableName}](";
                string ValuesStatement = "values(";
                var Params = new DynamicParameters();

                foreach(var i in DataMapping)
                {
                    if (Structure.Item2.Any(t => t.Equals(i.Key, StringComparison.OrdinalIgnoreCase)))
                    {
                        InsertStatement += $"[{i.Key}]";
                        ValuesStatement += $"@{i.Key}";
                        Params.Add($"@{i.Key}", new DbString { Value = i.Value.ToString(), IsAnsi = true });
                    }
                }

                InsertStatement += ")";
                ValuesStatement += ")";
                var Statement = $"{InsertStatement} {ValuesStatement}";
                Connection.Query(Statement,Params);
            }
        }

        private static Tuple<TableStructure,List<string>> GetStructure(SqlConnection Connection, string TypeName)
        {
            var Tables = Connection.Query<TableStructure>(@"select  tbl.name as TableName, sc.name as SchemaName from sys.tables tbl with(nolock)
                join sys.schemas sc with(nolock)
                on sc.schema_id = tbl.schema_id");

            if (!Tables.Any(t => t.TableName.Contains(TypeName, StringComparison.OrdinalIgnoreCase)))
                throw new ArgumentException("The table name does not match...");

            if (Tables.Where(t => t.TableName.Equals(TypeName, StringComparison.OrdinalIgnoreCase)).Count() > 1)
                throw new Exception("Too many table names is matching. You may need to use the overloaded method to do explicit mapping.");

            var Table = Tables.Where(t => t.TableName.Equals(TypeName,StringComparison.OrdinalIgnoreCase)).First();

            return new Tuple<TableStructure, List<string>>(Table ,Connection.Query<string>(@"select  cl.name from sys.columns cl with(nolock)
                    join sys.tables tbl with(nolock)
                    on tbl.object_id = cl.object_id
                    where tbl.name = @TableName", new { TableName = new DbString { IsAnsi = true, Value = Table.TableName } }).ToList());
        }

    }
}
