﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;

namespace HLMonitoring.Dao
{
    public sealed class DaoHelper : IDisposable
    {
        private SqlConnection Connection = null;

        public void Dispose()
        {
            Connection.Dispose();
            Connection = null;
        }

        public SqlConnection GetSqlConnection(string Database)
        {

            var _ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings[Database].ConnectionString;

            Connection = new SqlConnection()
            {

                ConnectionString = _ConnectionString

            };

            Connection.Open();

            return Connection;

        }
    }
}
