﻿using HLMonitoring.Monitoring;
using HLMonitoring.Notifier;
using HLMonitoring.Runner;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace HLMonitoring
{
    public partial class HLMonitoring : ServiceBase
    {
        List<Task> Tasks;
        bool StopService = false;
        public HLMonitoring()
        {
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {

            try
            {
                Tasks = new List<Task>();
                // Register the monitoring products
                MonitoringFactory.Register();


                if (Properties.Settings.Default.EnableFileLogging == true)
                    Logger.Logger.LogToFile(new { StartAction = "Service Started, and Factory is registered..." });

                //Create the tasks
                foreach (IProduct p in MonitoringFactory.ProductsToMonitor)
                {
                    foreach (Monitoring.IComponent c in p.Components)
                    {
                        if (Properties.Settings.Default.EnableFileLogging == true)
                            Logger.Logger.LogToFile(new { Action = $"Entered into Component check state. Checking Component: {c.GetType().Name}" });

                        Tasks.Add(Task.Factory.StartNew(() =>
                        {
                            while (!StopService)
                                new Run().RunTask(c, 5, c.GetType().Name == "InformzErrorLogs" ? 240000 : 120000);
                        }));

                    };
                }

                Task.WhenAll(Tasks);

            }
            catch (Exception e)
            {
                Logger.Logger.LogToFile(new { Action = $"Exception Occurred: {e.ToString()}" });
                Thread.Sleep(10000);
            }

        }

        protected override void OnStop()
        {
            StopService = true;
            Tasks.Clear();
            MonitoringFactory.DeRegister();
        }

    }
}
