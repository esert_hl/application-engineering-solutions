﻿using HLMonitoring.Monitoring;
using HLMonitoring.Notifier;
using HLMonitoring.Runner;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace HLMonitoring
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>       
        static void Main()
        {

#if DEBUG
            List<Task> Tasks;
            bool StopService = false;
            try
            {
                Tasks = new List<Task>();
                // Register the monitoring products
                MonitoringFactory.Register();
                var t = (int?)long.Parse((3935222697).ToString());
                if (Properties.Settings.Default.EnableFileLogging == true)
                    Logger.Logger.LogToFile(new { StartAction = "Service Started, and Factory is registered..." });

                //Create the tasks
                foreach (IProduct p in MonitoringFactory.ProductsToMonitor)
                {
                    foreach (Monitoring.IComponent c in p.Components)
                    {
                        if (Properties.Settings.Default.EnableFileLogging == true)
                            Logger.Logger.LogToFile(new { Action = $"Entered into Component check state. Checking Component: {c.GetType().Name}" });

                        
                        new Run().RunTask(c, 5, c.GetType().Name == "InformzErrorLogs" ? 240000 : 120000);
                        

                    };
                }
                

            }
            catch (Exception e)
            {
                Logger.Logger.LogToFile(new { Action = $"Exception Occurred: {e.ToString()}" });
            }

#else
            ServiceBase[] ServicesToRun;
            ServicesToRun = new ServiceBase[]
            {
                new HLMonitoring()
            };
            ServiceBase.Run(ServicesToRun);
#endif

        }

    }
}
