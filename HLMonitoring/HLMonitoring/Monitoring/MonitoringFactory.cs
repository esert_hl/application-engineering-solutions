﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HLMonitoring.Monitoring
{
    public static class MonitoringFactory 
    {
        internal static List<IProduct> ProductsToMonitor { get; private set; }
        public static void Register()
        {
            ProductsToMonitor = new List<IProduct>();
            ProductsToMonitor.Add(new Informz());
            ProductsToMonitor.Add(new RealMagnet());
            
        }
        
        public static void DeRegister()
        {
            ProductsToMonitor.Clear();
        }
    }
}
