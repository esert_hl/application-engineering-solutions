﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HLMonitoring.Monitoring
{
    internal interface IMonitor
    {
        void Run(IProduct Prod);

    }
}
