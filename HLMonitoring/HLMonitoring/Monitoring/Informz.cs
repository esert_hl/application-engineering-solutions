﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Caching;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using Extensions;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace HLMonitoring.Monitoring
{
    internal sealed class Informz : IProduct
    {
        public List<IComponent> Components { get; set; }
        public Informz()
        {
            Components = new List<IComponent>();
            Components.Add(new InformzErrorLogs());
        }

        private class InformzErrorLogs : IComponent
        {
            public InformzErrorLogs()
            {

            }
            private Status _Status = new Status() { Health = HealthStatus.Healthy, Message = "" };
            public Status GetStatus()
            {
                return _Status;
            }

            private class SysWebError
            {
                public long web_error_id { get; set; }
                public long subscriber_id { get; set; }
                public string url { get; set; }
                public DateTime web_error_date { get; set; }
                public string msg { get; set; }
                public string error_type { get; set; }
                public string web_page { get; set; }
                public int error_status { get; set; }
                public string server_name { get; set; }
            }

            private class SystemError
            {
                public int SYSTEM_ERROR_ID { get; set; }
                public string SYSTEM_ERROR_WHO { get; set; }
                public string SYSTEM_ERROR_LOCATION { get; set; }
                public string SYSTEM_ERROR_DESCRIPTION { get; set; }
                public int SYSTEM_ERROR_SEVERITY { get; set; }
                public DateTime SYSTEM_ERROR_DATETIME { get; set; }
                public int BRAND_ID { get; set; }
                public int MAILING_ID { get; set; }
                public string SYSTEM_ERROR_TYPE { get; set; }
                public int show_filtered { get; set; }

            }

            private class ErrorPattern
            {
                public string Error { get; set; }
                public int ErrorCount { get; set; }
            }

            public void RunHealthCheck()
            {
                _Status.Message = "";
                _Status.Health = HealthStatus.Healthy;

                var InformzDatabases = new string[] { "HLMonitoring.Properties.Settings.DB01", "HLMonitoring.Properties.Settings.DB02", "HLMonitoring.Properties.Settings.DB03", "HLMonitoring.Properties.Settings.DB04" };

                foreach (var DB in InformzDatabases)
                {
                    using (var Connection = new Dao.DaoHelper().GetSqlConnection(DB))
                    {
                        var Pattern = new List<ErrorPattern>();
                        var Cached = Cachebility.Caching.GetCachedObject<List<ErrorPattern>>(DB);

                        if (Cached == null)
                        {
                            var InformzErrorPatterns = Connection.Query<ErrorPattern>(@"WITH CTE AS(
                            Select LEFT(SYSTEM_ERROR_DESCRIPTION,250) as Error, count(*) as ErrorCount from  dbo.System_Error se with(nolock) where se.SYSTEM_ERROR_DATETIME > DATEADD(MONTH,-1,GETDATE())
                            and se.SYSTEM_ERROR_DATETIME < DATEADD(DAY,-3,GETDATE())
                            and (se.SYSTEM_ERROR_WHO <> 'LogThreadMessage Method' and se.SYSTEM_ERROR_LOCATION <> 'eMailConnection' )
                            group by LEFT(SYSTEM_ERROR_DESCRIPTION,250))
                            Select  * from CTE ct
                            where ct.ErrorCount > 100");

                            //Query the errors
                            Cachebility.Caching.CacheObject(DB, InformzErrorPatterns.ToList(), 12);
                            Cached = Cachebility.Caching.GetCachedObject<List<ErrorPattern>>(DB);
                        }
                        
                        Pattern = Cached as List<ErrorPattern>;


                        var SystemErrors = Connection.Query<SystemError>(@"Select * from  dbo.System_Error se with(nolock) where se.SYSTEM_ERROR_DATETIME > DATEADD(HOUR,-1,GETDATE())").Where(
                                t => Pattern.Any(i => t.SYSTEM_ERROR_DESCRIPTION.Contains(i.Error, StringComparison.OrdinalIgnoreCase) == false)
                            ).ToList();

                        var WebErrors = Connection.Query<SysWebError>(@"Select * from dbo.informz_sys_web_error sw with(nolock) where sw.web_error_date > DATEADD(HOUR,-1,GETDATE())").Where(t => Pattern.Any(i => t.msg.Contains(i.Error, StringComparison.OrdinalIgnoreCase) == false)).ToList();
                        
                        var Errors = JObject.Parse(File.ReadAllText(@"config.json"));
                        foreach (string ErrorToIgnore in (JArray)Errors["IgnoreError"])
                        {
                            SystemErrors.RemoveAll(t => t.SYSTEM_ERROR_DESCRIPTION.Contains(ErrorToIgnore, StringComparison.OrdinalIgnoreCase));
                            WebErrors.RemoveAll(t=>t.msg.Contains(ErrorToIgnore,StringComparison.OrdinalIgnoreCase));
                        }
                        
                        _Status.Message += Environment.NewLine + $"Within the last hour there has been {WebErrors.Count()} web errors, and {SystemErrors.Count()} system errors in total for {DB.Split(new[] { "HLMonitoring.Properties.Settings." }, StringSplitOptions.None)[1]}." + Environment.NewLine;

                        if (SystemErrors.Any())
                        {
                            var SystemErrorCount = SystemErrors.GroupBy(t => new { t.BRAND_ID, t.SYSTEM_ERROR_WHO, t.SYSTEM_ERROR_LOCATION }).Select(a => new { BrandId = a.Key, Count = a.Count() });

                            foreach (var E in SystemErrorCount)
                            {
                                if (E.Count > 50 && E.BrandId.BRAND_ID == -1)
                                {
                                    _Status.Health = HealthStatus.Fault;
                                    _Status.Message += $"Brand Id:{E.BrandId.BRAND_ID}, Error Location: {E.BrandId.SYSTEM_ERROR_LOCATION}, Who: {E.BrandId.SYSTEM_ERROR_WHO} has experienced {E.Count} errors within the last hour." + Environment.NewLine;
                                }

                                if (E.Count > 50 && E.BrandId.BRAND_ID != -1)
                                {
                                    _Status.Health = HealthStatus.Fault;
                                    _Status.Message += $"Brand Id:{E.BrandId.BRAND_ID}, Error Location: {E.BrandId.SYSTEM_ERROR_LOCATION}, Who: {E.BrandId.SYSTEM_ERROR_WHO} has experienced {E.Count} errors within the last hour." + Environment.NewLine;
                                }

                            }
                        }
                        
                        if (WebErrors.Any())
                        {
                            var WebErrorCount = (from i in WebErrors
                                                 group WebErrors by i.subscriber_id
                                                into t
                                                 select new { SubscriberId = t.Key, Count = t.Count() });


                            foreach (var We in WebErrorCount)
                            {
                                if (We.Count > 50)
                                {
                                    _Status.Health = HealthStatus.Fault;
                                    _Status.Message += $"Subscriber id {We.SubscriberId} has experienced {We.Count} errors within the last hour." + Environment.NewLine;
                                }
                            }
                        }

                    }
                }
            }
        }
    }
}
