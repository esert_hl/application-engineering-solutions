﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HLMonitoring.Monitoring
{
    internal interface IProduct
    {
        List<IComponent> Components { get; set; }
    }

    public interface IComponent
    {
        void RunHealthCheck();
        Status GetStatus();
    }

    public sealed class Status
    {
        public HealthStatus Health { get; set; }
        public string Message { get; set; }
    }

    public enum HealthStatus
    {
        Warning,
        Fault,
        Healthy
    }
}
