﻿using HLMonitoring.Notifier;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HLMonitoring.Runner
{
    public class FaultNotification
    {
        public static EmailConfiguration EConfiguration = new EmailConfiguration()
        {
            Server = "10.7.55.13",
            Port = 587,
            UserName = "support_code@magnetmail.net",
            UseSSL = true,
            Password = "DUkF8GUgAT"

        };

        public List<Contact> Contacts = new List<Contact>() {
                new Contact(){
                    Email = Properties.Settings.Default.BusinessHoursEmail,
                    //Email = "application-engineering-monitor-business-hours@higherlogic.pagerduty.com",
                    EmailGroup = EmailGroup.BusinessHours
                },
                new Contact()
                {
                    Email = Properties.Settings.Default.CriticalEmail,
                    //Email = "application-engineering-monitor-critical@higherlogic.pagerduty.com",
                    EmailGroup = EmailGroup.Urgent
                }
            };
        
    }
}
