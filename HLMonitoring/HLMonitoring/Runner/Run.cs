﻿using HLMonitoring.Monitoring;
using HLMonitoring.Notifier;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace HLMonitoring.Runner
{
    internal class Run
    {
        public Run()
        {
            Notify.LoadConfiguration(FaultNotification.EConfiguration, IsBusinessHours() ? new FaultNotification().Contacts.Where(t=>t.EmailGroup == EmailGroup.BusinessHours).ToList() : new FaultNotification().Contacts.Where(t=>t.EmailGroup == EmailGroup.Urgent).ToList());
        }

        public void RunTask(IComponent Component, int Retry, int ToSleep)
        {
            System.IO.Directory.SetCurrentDirectory(System.AppDomain.CurrentDomain.BaseDirectory);
            try
            {
                Logger.Logger.LogToFile(new { Action = $"Entered into Component check state. Checking Component: {Component.GetType().Name}" });
                Component.RunHealthCheck();
                Status Status = Component.GetStatus();

                if (!IsBusinessHours())
                    ToSleep *= 5;

                if (Retry > 0 && Status.Health == HealthStatus.Fault)
                {
                    Logger.Logger.LogToFile(new { Action = $"Fault was found at: {Component.GetType().Name}.{Environment.NewLine}Error:{Status.Message}. Will try again in {ToSleep / 1000} seconds" });
                    Thread.Sleep(ToSleep);

                    while (Retry > 0 && Status.Health == HealthStatus.Fault)
                    {
                        Logger.Logger.LogToFile(new { Action = $"Fault was found at: {Component.GetType().Name}.{Environment.NewLine}Error:{Status.Message}. Will try again in {ToSleep / 1000} seconds" });
                        Component.RunHealthCheck();
                        Status = Component.GetStatus();
                        Retry -= 1;
                        Thread.Sleep(ToSleep);
                    }

                    if (Retry == 0 && Status.Health == HealthStatus.Fault)
                    {
                        var EmailContent = new Content()
                        {
                            EmailBody = $"{Status.Message}",
                            FromAddress = "hlmonitoring@higherlogic.com",
                            Subject = $"Fault at {Component.GetType().Name}. Please check the error logs"
                        };

                        Notify.NotifyContacts(EmailContent);                        
                    }

                    Logger.Logger.LogToFile(new { Action = $"Status of {Component.GetType().FullName}:{Status.Message}" });
                }

                Thread.Sleep(ToSleep);

            }catch(Exception e)
            {
                Logger.Logger.LogToFile(new { Action = $"Exception Occurred: {e.ToString()}" });

                Thread.Sleep(ToSleep);
                
            }
        }

        private bool IsBusinessHours()
        {
            //Define business hours 8 AM to 6PM EST.
            var UTCNow = DateTime.UtcNow;

            TimeZoneInfo ESTZone = TimeZoneInfo.FindSystemTimeZoneById("Eastern Standard Time");

            DateTime ESTNow = TimeZoneInfo.ConvertTimeFromUtc(UTCNow, ESTZone);

            if (ESTNow.DayOfWeek == DayOfWeek.Sunday || ESTNow.DayOfWeek == DayOfWeek.Saturday)
                return false;

            TimeSpan Start = TimeSpan.Parse("08:00");
            TimeSpan End = TimeSpan.Parse("18:00");

            return ESTNow.TimeOfDay >= Start && ESTNow.TimeOfDay <= End;
        }
    }
}
