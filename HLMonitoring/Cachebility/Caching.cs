﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Caching;
using System.Text;
using System.Threading.Tasks;

namespace Cachebility
{
    public static class Caching
    {
        private static ObjectCache LocalCache = new MemoryCache("Global");
        public static void CacheObject<T>(string key,T Object,int TimeToLive)
        {
            var Obj = LocalCache[key] as object;

            if (Obj == null)
                LocalCache.Add(key,Object, DateTime.Now.Add(new TimeSpan(TimeToLive,0,0)));

        }

        public static T GetCachedObject<T>(string key)
        {
            var Val = LocalCache[key] as object;
            return (T)Val;
        }

    }
}
